import store from "./../store";

const ifNotAuthenticated = (to, from, next) => {
  if (!store().getters['auth/isAuthenticated']) {
    next("/");
    return;
  }
  next();
};

const ifAuthenticated = (to, from, next) => {
  if (store().getters['auth/isAuthenticated']) {
    next("/");
    return;
  }
  next();
};
const routes = [
  {
    path: '/',
    component: () => import('layouts/FeedLayout.vue'),
    children: [
      { name: "feed", path: '/', component: () => import('pages/index.vue') },
      { name: 'about', path: '/about', component: () => import('pages/About.vue') },
      { name: "feedItem", path: '/feed/:feedId', component: () => import('pages/feedItemDetails.vue') },
      { name: "formItem", beforeEnter: ifNotAuthenticated, path: '/form', component: () => import('pages/feedItemForm.vue') }
    ]
  },
  {
    path: '/login',
    beforeEnter: ifAuthenticated,
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Login.vue') }
    ]
  },
  {
    path: '/register',
    beforeEnter: ifAuthenticated,
    component: () => import('layouts/RegisterLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Register.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
