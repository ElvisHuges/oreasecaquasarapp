import { api } from "./config";
import { getToken } from './../store/utils/token'

export default {

    login(username, password) {
        return api.post('/auth/signin/', { username: username, password: password })
    },
    register(username, fullname, email, password) {
        return api.post('/auth/signup/', { username: username, fullname: fullname, email: email, password: password })
    },

    getPosts() {
        return api.get('/posts')
    },
    createPost(title, description, andress, status, file) {
        return api.post("/posts", file, {
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-access-token': getToken()
            },
            params: {
                title: title, description: description, andress: andress, status: status, file: file
            }
        });
    },
}