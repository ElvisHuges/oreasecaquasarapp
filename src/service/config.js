var axios = require('axios');
import { getToken } from './../store/utils/token'

export const baseURL = "http://localhost:3000/"  // local database acess
//export const baseURL = "http://api.bocapio.com:3000/api/v1/"

export const api = axios.create({
    baseURL: baseURL,
    /* other custom settings */
    headers: {
        'Access-Control-Allow-Origin': '*',
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'x-access-token': getToken()
    }
})
