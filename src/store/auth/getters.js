
const isAuthenticated = (state) => {
    return !!state.accessToken
}
const acessToken = (state) => {
    state.accessToken
}
const user = (state) => {
    state.user
}
export {
    isAuthenticated,
    acessToken,
    user
}