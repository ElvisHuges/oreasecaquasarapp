import api from "./../../service/api.service";
import { getToken, removeToken, setToken } from '../utils/token'

const registerAuth = ({ commit }, payload) => {
  console.log("payload", payload);
  return new Promise((resolve, reject) => {
    api.register(payload.username, payload.fullname, payload.email, payload.password)
      .then(rsp => {
        console.log("#RSP REGISTER#", rsp);
        setToken(rsp.data.token)
        commit("REGISTER_SUCESS", rsp.data)
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
  });
}

const loginAuth = ({ commit }, payload) => {
  console.log("payload", payload);
  return new Promise((resolve, reject) => {
    api.login(payload.username, payload.password)
      .then(rsp => {
        console.log("#RSP LOGIN#", rsp);
        setToken(rsp.data.token)
        commit("LOGIN_SUCESS", rsp.data)
        resolve(true);
      })
      .catch(err => {
        reject(err);
      });
  });
}

const logoutAuth = ({ commit }) => {
  removeToken()
  commit("LOGOUT_SUCESS")
}

export {
  registerAuth,
  logoutAuth,
  loginAuth
}