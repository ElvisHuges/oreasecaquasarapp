
const REGISTER_SUCESS = (state, payload) => {
      state.accessToken = payload.token;
      state.user = payload.user
}
const LOGIN_SUCESS = (state, payload) => {
      state.accessToken = payload.token;
      state.user = payload.user
}

const LOGOUT_SUCESS = (state, payload) => {
      state.accessToken = '';
      state.user = ''
}

const REGISTER_ERROR = (state, payload) => {
      state.error = true;
}

export {
      REGISTER_SUCESS,
      REGISTER_ERROR,
      LOGIN_SUCESS,
      LOGOUT_SUCESS
}
