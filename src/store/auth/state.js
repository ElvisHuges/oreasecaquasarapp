import { getToken, removeToken, setToken } from '../utils/token'

export default {
  accessToken: getToken(),
  user: {
    id: "",
    name: "",
    username: "",
    email: ""
  },
}
